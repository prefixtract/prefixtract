# prefiXtract

Prefix Feature Extractor for QBF formulas in QDIMACS format. Extracts features
of the QDIMACS prefix according to "Portfolio Based Algorithm-Selection for
Circuit QBFs".

## Usage

Supply paths to PCNF formulas in QDIMACS format and get statistics. Pipe into
`column -t` to columnize the output nicely.

Example:

```
$ ./prefiXtract ../tests/*.qdimacs | column -t
name                        nr_of_existential_variables  nr_of_universal_variables  balance_ex_univ  nr_of_quantifier_blocks  min_size_of_quantifier_block  max_size_of_quantifier_block  average_size_of_quantifier_block  sd_of_quantifier_block
"../tests/small-2.qdimacs"  2                            2                          2.000000         2                        2                             2                             2.000000                          0.000000
"../tests/small.qdimacs"    1                            2                          2.500000         2                        1                             2                             1.500000                          0.500000
```

You may also use prefiXtract to remove the prefix and see the file as regular
CNF in DIMACS format. To do this, supply `-p <outfile>`. Use `-p -` for stdout.
To deactivate statistics comments, supply `-c`. To deactivate full copy (e.g. to
only input the comments into a destination file), supply `-C`. This mechanism
also works for many files, but does not work with positional arguments, i.e.
supply filenames, outputnames and printnames as `-f`, `-p` (or `-o`), and `-n`.
With `-Z` the names of the columns are shortened and with `-A` the mean values of the columns are displayed in the last row.

Example:

```
$ ./prefiXtract ../tests/small.qdimacs -o - -n Small -s
c filename ../tests/small.qdimacs
c outname -
c printname Small
c nr_of_existential_variables 1
c nr_of_universal_variables 2
c balance_of_existential_and_universal_variables 2.500000
c nr_of_quantifier_blocks 2
c min_size_of_quantifier_block 1
c max_size_of_quantifier_block 2
c average_size_of_quantifier_block 1.500000
c sd_of_quantifier_block 0.500000
p cnf 3 2
1 2 0
-1 -2 0
```

## Building

```
mkdir build
cmake ..
make
```
