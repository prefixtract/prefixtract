#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef __FreeBSD__
#define __float128 double
#endif

static const char* long_field_names[] = { "filename",
                                          "name",
                                          "id",
                                          "nr_of_variables",
                                          "nr_of_clauses",
                                          "nr_of_existential_variables",
                                          "nr_of_universal_variables",
                                          "balance_ex_univ",
                                          "nr_of_quantifier_blocks",
                                          "min_size_of_quantifier_block",
                                          "max_size_of_quantifier_block",
                                          "average_size_of_quantifier_block",
                                          "sd_of_quantifier_block" };

static const char* short_field_names[] = { "f",   "n",     "id",    "#v",
                                           "#c",  "#E",    "#A",    "#E/#A",
                                           "#QB", "minQB", "maxQB", "avgQB",
                                           "sdQ" };

#define F_FILENAME 0
#define F_NAME 1
#define F_ID 2
#define F_NR_OF_VARIABLES 3
#define F_NR_OF_CLAUSES 4
#define F_NR_OF_EXISTENTIAL_VARIABLES 5
#define F_NR_OF_UNIVERSAL_VARIABLES 6
#define F_BALANCE_EX_UNIV 7
#define F_NR_OF_QUANTIFIER_BLOCKS 8
#define F_MIN_SIZE_OF_QUANTIFIER_BLOCK 9
#define F_MAX_SIZE_OF_QUANTIFIER_BLOCK 10
#define F_AVERAGE_SIZE_OF_QUANTIFIER_BLOCK 11
#define F_SD_OF_QUANTIFIER_BLOCK 12

static const char** field_names = long_field_names;

static int nrofclauses = 0;
static int nrofvars = 0;

// Unquantified variables must count towards existential variables.
static int nr_of_quantified_variables = 0;

// Features according to "Portfolio Based Algorithm-Selection for Circuit QBFs"
static int nr_of_existential_variables = 0;
static int nr_of_universal_variables = 0;
static double balance_ex_univ = 0;
static int nr_of_quantifier_blocks = 0;
static int min_size_of_quantifier_block = INT_MAX;
static int max_size_of_quantifier_block = 0;
static double average_size_of_quantifier_block = 0;
static double sd_of_quantifier_block = 0;

// Summed-up totals, used to compute arithmetic mean at the end.
static int total = 0;
static long total_nrofclauses = 0;
static long total_nrofvars = 0;
static long total_nr_of_existential_variables = 0;
static long total_nr_of_universal_variables = 0;
static long total_nr_of_quantifier_blocks = 0;
static double total_balance_ex_univ = 0;
static int total_min_size_of_quantifier_block = 0;
static int total_max_size_of_quantifier_block = 0;
static double total_average_size_of_quantifier_block = 0;
static double total_sd_size_of_quantifier_block = 0;

static bool debug = false;

static bool print_full_statistics = true;
static bool print_arith_mean_at_end = false;

static char last_c = ' ';

static char quantifier = '_', last_quantifier = '_';
static __float128 sum_of_quantifier_blocks = 0;
static __float128 sum_of_squared_quantifier_blocks = 0;

static int current_quantifier_size = 0;

static void
sum_stats_to_total() {
  ++total;

  total_nrofclauses += nrofclauses;
  total_nrofvars += nrofvars;
  total_nr_of_existential_variables += nr_of_existential_variables;
  total_nr_of_universal_variables += nr_of_universal_variables;
  total_balance_ex_univ += balance_ex_univ;
  total_nr_of_quantifier_blocks += nr_of_quantifier_blocks;
  total_average_size_of_quantifier_block += average_size_of_quantifier_block;
  total_min_size_of_quantifier_block += min_size_of_quantifier_block;
  total_max_size_of_quantifier_block += max_size_of_quantifier_block;
  total_sd_size_of_quantifier_block += sd_of_quantifier_block;
}

static void
print_arithmetic_means_from_totals() {
  printf("MEAN\t-1\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf",
         (double)total_nrofvars / total,
         (double)total_nrofclauses / total,
         (double)total_nr_of_existential_variables / total,
         (double)total_nr_of_universal_variables / total,
         (double)total_balance_ex_univ / total,
         (double)total_nr_of_quantifier_blocks / total);

  if(print_full_statistics)
    printf("\t%lf\t%lf\t%lf\t%lf\n",
           (double)total_min_size_of_quantifier_block / total,
           (double)total_max_size_of_quantifier_block / total,
           (double)total_average_size_of_quantifier_block / total,
           (double)total_sd_size_of_quantifier_block / total);

  printf("\n");
}

static void
reset_global_stats() {
  nrofclauses = 0;
  nrofvars = 0;

  nr_of_quantified_variables = 0;

  nr_of_existential_variables = 0;
  nr_of_universal_variables = 0;
  balance_ex_univ = 0;
  nr_of_quantifier_blocks = 0;
  min_size_of_quantifier_block = INT_MAX;
  max_size_of_quantifier_block = 0;
  average_size_of_quantifier_block = 0;
  sd_of_quantifier_block = 0;

  last_c = ' ';

  quantifier = '_';
  last_quantifier = '_';
  sum_of_quantifier_blocks = 0;
  sum_of_squared_quantifier_blocks = 0;
  current_quantifier_size = 0;
}

// "Standard Deviation in One Pass"
// https://www.strchr.com/standard_deviation_in_one_pass?allcomments=1
// double std_dev2(double a[], int n) {
//     if(n == 0)
//         return 0.0;
//     double sum = 0;
//     double sq_sum = 0;
//     for(int i = 0; i < n; ++i) {
//        sum += a[i];
//        sq_sum += a[i] * a[i];
//     }
//     double mean = sum / n;
//     double variance = sq_sum / n - mean * mean;
//     return sqrt(variance);
// }

static inline void
file_line(FILE* f, const char* format, va_list ap) {
  va_list args;
  vfprintf(f, format, ap);
  fprintf(f, "\n");
}

void
dbg(const char* format, ...) {
  if(debug) {
    va_list args;
    va_start(args, format);
    file_line(stderr, format, args);
    va_end(args);
  }
}

void
err(const char* format, ...) {
  va_list args;
  va_start(args, format);
  fprintf(stderr, "!! ");
  file_line(stderr, format, args);
  va_end(args);
}

void
out(const char* format, ...) {
  va_list args;
  va_start(args, format);
  file_line(stdout, format, args);
  va_end(args);
}

void
comment(FILE* f, const char* format, ...) {
  fprintf(f, "c ");
  va_list args;
  va_start(args, format);
  file_line(f, format, args);
  va_end(args);
}

int
next(FILE* file, uint64_t* linenumber, int c) {
  c = getc(file);
  if(c == '\n')
    ++*linenumber;

  last_c = c;
  return c;
}

#define NEXT() next(file, linenumber, c)

int
next_after_space(FILE* file, uint64_t* linenumber, int c) {
  while((c == ' ' || c == '\t') && c != '\n' && c != EOF) {
    c = NEXT();
  }
  return c;
}

#define NEXT_AFTER_SPACE() next_after_space(file, linenumber, c)

const char*
next_int(FILE* file, uint64_t* linenumber, int* cp, int* outn) {
  bool neg = false;
  int c = *cp;
  if(c == '-')
    neg = true, c = NEXT();

  *cp = c;

  if(!isdigit(c)) {
    return "Encountered invalid digit!";
  }

  int n = c - '0';
  for(;;) {
    c = NEXT();
    *cp = c;
    if(isdigit(c)) {
      n *= 10;
      n += c - '0';
    } else
      break;
  }
  if(neg)
    n = -n;

  *outn = n;
  return NULL;
}

#define NEXT_INT(n)                                       \
  do {                                                    \
    const char* err = next_int(file, linenumber, &c, &n); \
    \ if(err) return err;                                 \
  } while(false)
#define NEXT_INT_AFTER_SPACE(n)                           \
  do {                                                    \
    c = NEXT_AFTER_SPACE();                               \
    const char* err = next_int(file, linenumber, &c, &n); \
    if(err)                                               \
      return err;                                         \
  } while(false)

static void
commit_quantifier_to_stats() {
  ++nr_of_quantifier_blocks;

  if(last_quantifier == 'e') {
    nr_of_existential_variables += current_quantifier_size;
  } else {
    nr_of_universal_variables += current_quantifier_size;
  }

  if(current_quantifier_size < min_size_of_quantifier_block) {
    min_size_of_quantifier_block = current_quantifier_size;
  }
  if(current_quantifier_size > max_size_of_quantifier_block) {
    max_size_of_quantifier_block = current_quantifier_size;
  }

  sum_of_quantifier_blocks += current_quantifier_size;
  long long int q = current_quantifier_size;
  sum_of_squared_quantifier_blocks += q * q;

  current_quantifier_size = 0;
}

const char*
parse_prefix(FILE* file, uint64_t* linenumber) {
  int c;

  int in_existential_block = true;
  int in_universal_block = true;
  int reading_quantifier = false;

  // Header
  for(;;) {
    c = NEXT();

    if(c == 'c') {
      while(c != '\n' && c != EOF)
        c = NEXT();
    } else if(c == 'p') {
      // Skip to problem line parser.
      break;
    }

    if(c == EOF)
      return "Empty file, could not find problem line!";
  }

  // Problem Definition
  assert(c == 'p');
  c = NEXT();
  c = NEXT_AFTER_SPACE();
  if(c != 'c')
    return "Problem line: c expected!";
  c = NEXT();
  if(c != 'n')
    return "Problem line: n expected!";
  c = NEXT();
  if(c != 'f')
    return "Problem line: f expected!";
  c = NEXT();

  NEXT_INT_AFTER_SPACE(nrofvars);
  NEXT_INT_AFTER_SPACE(nrofclauses);

  // Possibly Quantifiers
  for(;;) {
    c = NEXT_AFTER_SPACE();

    // Existential or universal block.
    if((c == 'e' || c == 'E')) {
      in_existential_block = true, in_universal_block = false,
      reading_quantifier = true, quantifier = 'e', c = NEXT();

      if(quantifier != last_quantifier && last_quantifier != '_') {
        commit_quantifier_to_stats();
      }
    } else if((c == 'a' || c == 'A')) {
      in_universal_block = true, in_existential_block = false,
      reading_quantifier = true, quantifier = 'a', c = NEXT();

      if(quantifier != last_quantifier && last_quantifier != '_') {
        commit_quantifier_to_stats();
      }
    } else if(c == '\n' && !reading_quantifier) {
      c = NEXT();
      continue;
    } else if(c == '\n' && reading_quantifier)
      return "Quantifier block was not closed with a 0!";

    // Quantifier block is done.
    else if((isdigit(c) || c == '-') && !reading_quantifier)
      break;
    else if(c == EOF)
      break;
    else if(!reading_quantifier)
      return "Some issue in the quantifier prefix ordering!";

    int v;
    NEXT_INT_AFTER_SPACE(v);

    if(v == 0) {
      // End current quantifier!
      reading_quantifier = false;
      last_quantifier = quantifier;
    } else {
      // Handle quantified variable.
      ++nr_of_quantified_variables;
      ++current_quantifier_size;
    }
  }

  if(quantifier != '_')
    commit_quantifier_to_stats();

  int unquantified_variables = nrofvars - nr_of_quantified_variables;
  if(unquantified_variables > 0)
    nr_of_existential_variables += unquantified_variables;

  __float128 mean = sum_of_quantifier_blocks / nr_of_quantifier_blocks;
  __float128 variance =
    sum_of_squared_quantifier_blocks / nr_of_quantifier_blocks - mean * mean;
  average_size_of_quantifier_block = mean;

  if(nr_of_existential_variables > 0 && nr_of_universal_variables > 0)
    balance_ex_univ =
      ((double)nr_of_existential_variables /
       (double)nr_of_universal_variables) +
      ((double)nr_of_universal_variables / (double)nr_of_existential_variables);

  sd_of_quantifier_block = sqrt(variance);

  // Ignore the rest.
  return NULL;
}

void
copy_rest_of_file(FILE* in, FILE* out) {
  for(;;) {
    int c = getc(in);
    if(c == EOF)
      break;
    else
      putc(c, out);
  }
}

void
print_stats_comments(const char* filename,
                     const char* printname,
                     const char* outname,
                     FILE* o) {
  comment(o, "%s %s", field_names[F_FILENAME], filename);
  comment(o, "outname %s", outname);
  comment(o, "printname %s", printname);
  comment(o, "%s %d", field_names[F_NR_OF_VARIABLES], nrofvars);
  comment(o, "%s %d", field_names[F_NR_OF_CLAUSES], nrofclauses);
  comment(o,
          "%s %d",
          field_names[F_NR_OF_EXISTENTIAL_VARIABLES],
          nr_of_existential_variables);
  comment(o,
          "%s %d",
          field_names[F_NR_OF_UNIVERSAL_VARIABLES],
          nr_of_universal_variables);
  comment(o, "%s %lf", field_names[F_BALANCE_EX_UNIV], balance_ex_univ);
  comment(o,
          "%s %d",
          field_names[F_NR_OF_QUANTIFIER_BLOCKS],
          nr_of_quantifier_blocks);
  comment(o,
          "%s %d",
          field_names[F_MIN_SIZE_OF_QUANTIFIER_BLOCK],
          min_size_of_quantifier_block);
  comment(o,
          "%s %d",
          field_names[F_MAX_SIZE_OF_QUANTIFIER_BLOCK],
          max_size_of_quantifier_block);
  comment(o,
          "%s %lf",
          field_names[F_AVERAGE_SIZE_OF_QUANTIFIER_BLOCK],
          average_size_of_quantifier_block);
  comment(
    o, "%s %lf", field_names[F_SD_OF_QUANTIFIER_BLOCK], sd_of_quantifier_block);
}

void
print_statsline_header() {
  printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
         field_names[F_NAME],
         field_names[F_ID],
         field_names[F_NR_OF_VARIABLES],
         field_names[F_NR_OF_CLAUSES],
         field_names[F_NR_OF_EXISTENTIAL_VARIABLES],
         field_names[F_NR_OF_UNIVERSAL_VARIABLES],
         field_names[F_BALANCE_EX_UNIV],
         field_names[F_NR_OF_QUANTIFIER_BLOCKS]);
  if(print_full_statistics)
    printf("\t%s\t%s\t%s\t%s",
           field_names[F_MIN_SIZE_OF_QUANTIFIER_BLOCK],
           field_names[F_MAX_SIZE_OF_QUANTIFIER_BLOCK],
           field_names[F_AVERAGE_SIZE_OF_QUANTIFIER_BLOCK],
           field_names[F_SD_OF_QUANTIFIER_BLOCK]);
  printf("\n");
}

void
print_statsline(const char* printname, uint32_t id) {
  if(print_full_statistics)
    printf("\"%s\"\t%d\t%d\t%d\t%d\t%d\t%lf\t%d\t%d\t%d\t%lf\t%lf\n",
           printname,
           id,
           nrofvars,
           nrofclauses,
           nr_of_existential_variables,
           nr_of_universal_variables,
           balance_ex_univ,
           nr_of_quantifier_blocks,
           min_size_of_quantifier_block,
           max_size_of_quantifier_block,
           average_size_of_quantifier_block,
           sd_of_quantifier_block);
  else
    printf("\"%s\"\t%d\t%d\t%d\t%d\t%d\t%lf\t%d\n",
           printname,
           id,
           nrofvars,
           nrofclauses,
           nr_of_existential_variables,
           nr_of_universal_variables,
           balance_ex_univ,
           nr_of_quantifier_blocks);
}

static void
print_help() {
  printf("prefiXtract [filename 1] [filename 2] [...]\n");
  printf("SOURCE:\n");
  printf("    https://gitlab.sai.jku.at/qbf/software/prefixtract\n");
  printf("USAGE:\n");
  printf("    -h\t\tPrint help.\n");
  printf("    -h\t\tPrint version.\n");
  printf("    -f\t\tFilename to read. Also positional. If more than one is "
         "supplied, no output may be generated. - reads from stdin.\n");
  printf("    -p / -o\tRemove the prefix and pipe CNF into other file. - for "
         "stdout.\n");
  printf("    -s\t\tDo not print the statsline.\n");
  printf("    -S\t\tPrint less statistics (more compact output).\n");
  printf("    -Z\t\tmake field names more compact (zip it!).\n");
  printf("    -n\t\tExplicitly specify a name to print for the statsline.\n");
  printf("    -e\t\tDo NOT print header of statsline.\n");
  printf("    -c\t\tDo not write stats comments (requires -p)\n");
  printf("    -A\t\tAdd a line at the end giving arithmetic means.\n");
  printf("    -C\t\tDo not copy full QDIMACS into DIMACS in output file "
         "(requires -p)\n");
  printf("    -d\t\tActivate debug mode\n");
}

static void
print_version() {
  printf("prefiXtract version 1.0\n");
  printf("  (c) 2021-2023 Max Heisinger @ JKU <maximilian.heisinger@jku.at>\n");
}

static int
dofile(const char* infile_path,
       const char* printname,
       const char* outfile_path,
       uint32_t id,
       bool copy,
       bool remove_prefix,
       bool statsline,
       bool comments) {
  static bool stdin_read = false;
  bool infile_close = false;
  bool outfile_close = false;
  FILE* infile = NULL;
  FILE* outfile = NULL;

  assert(infile_path);
  reset_global_stats();

  if(strcmp(infile_path, "-") == 0) {
    infile = stdin;
    printname = "stdin";
    if(stdin_read) {
      err("Cannot read from stdin more than once!");
      return EXIT_FAILURE;
    } else {
      dbg("Input file for PCNF input: <stdin>");
      stdin_read = true;
    }
  } else {
    infile = fopen(infile_path, "r");
    if(!infile) {
      err("Could not open file \"%s\" for reading! Error: %s",
          infile_path,
          strerror(errno));
      return EXIT_FAILURE;
    }
    infile_close = true;
  }

  if(outfile_path) {
    if(strcmp(outfile_path, "-") == 0) {
      outfile = stdout;
      dbg("Destination file for CNF output without the prefix: <stdout>");
    } else {
      dbg("Destination file for CNF output without the prefix: \"%s\"",
          outfile_path);
      outfile = fopen(outfile_path, "w");
      if(!outfile) {
        err("Could not open destination file \"%s\" for writing! Error: %s",
            outfile_path,
            strerror(errno));
        return EXIT_FAILURE;
      }
      outfile_close = true;
    }
  }

  uint64_t linenumber = 1;
  const char* error = parse_prefix(infile, &linenumber);
  if(error) {
    err("\"%s\"\tEncountered error on line %llu: %s",
        infile_path,
        linenumber,
        error);
    return EXIT_FAILURE;
  }

  if(outfile) {
    // Create a new file with same contents but without the prefix!
    if(comments) {
      print_stats_comments(infile_path, printname, outfile_path, outfile);
    }
    if(copy) {
      fprintf(outfile, "p cnf %d %d\n", nrofvars, nrofclauses);
      putc(last_c, outfile);
      copy_rest_of_file(infile, outfile);
    }
  }

  if(statsline) {
    print_statsline(printname, id);
  }

  if(infile && infile_close) {
    fclose(infile);
  }

  if(outfile && outfile_close) {
    fclose(outfile);
  }

  sum_stats_to_total();

  return EXIT_SUCCESS;
}

int
main(int argc, char* argv[]) {
  const char* outfile_path = NULL;
  bool statsline = true;
  bool statsline_header = true;
  bool comments = true;
  bool copy = true;
  char c;

  bool remove_prefix = false;

  const char** filenames = NULL;
  const char** printnames = NULL;
  const char** outnames = NULL;
  int filenames_count = 0;

  int printname_before = 0;
  int outname_before = 0;

  while((c = getopt(argc, argv, "n:escChvpo:df:SZA")) != -1) {
    switch(c) {
      case 'h':
        print_help();
        return EXIT_SUCCESS;
      case 'v':
        print_version();
        return EXIT_SUCCESS;
      case 'e':
        statsline_header = false;
        break;
      case 'A':
        print_arith_mean_at_end = true;
        break;
      case 'S':
        print_full_statistics = false;
        break;
      case 'Z':
        field_names = short_field_names;
        break;
      case 'p':
      case 'o':
        if(filenames_count == 0) {
          if(outname_before == 0) {
            outname_before = 1;
            outnames = malloc(sizeof(const char*));
          } else {
            err("Cannot have more than one outname per file!");
            return EXIT_FAILURE;
          }
        }
        outnames[filenames_count - 1 + outname_before] = optarg;
        break;
      case 'n':
        if(filenames_count == 0) {
          if(printname_before == 0) {
            printname_before = 1;
            printnames = malloc(sizeof(const char*));
          } else {
            err("Cannot have more than one printname per file!");
            return EXIT_FAILURE;
          }
        }
        printnames[filenames_count - 1 + printname_before] = optarg;
        break;
      case 'c':
        comments = false;
        break;
      case 'C':
        copy = false;
        break;
      case 'd':
        debug = true;
        break;
      case 'f':
        filenames = realloc(filenames, sizeof(const char*) * ++filenames_count);
        printnames =
          realloc(printnames,
                  sizeof(const char*) * (filenames_count + printname_before));
        outnames = realloc(
          outnames, sizeof(const char*) * (filenames_count + printname_before));
        filenames[filenames_count - 1] = optarg;
        printnames[filenames_count - 1 + printname_before] = optarg;
        outnames[filenames_count - 1 + outname_before] = NULL;
        break;
      case '?':
        if(optopt == 'c')
          fprintf(stderr, "Option -%c requires an argument.\n", optopt);
        else if(isprint(optopt))
          fprintf(stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
        return EXIT_FAILURE;
      default:
        abort();
    }
  }

  for(size_t i = optind; i < argc; ++i) {
    // Positional argument (filename/s)
    filenames_count += 1;
    filenames = realloc(filenames, sizeof(const char*) * filenames_count);
    printnames = realloc(
      printnames, sizeof(const char*) * (filenames_count + printname_before));
    outnames = realloc(
      outnames, sizeof(const char*) * (filenames_count + outname_before));
    filenames[filenames_count - 1] = argv[i];
    printnames[filenames_count - 1 + printname_before] = argv[i];
    outnames[filenames_count - 1 + outname_before] = NULL;
  }

  if(statsline_header && statsline) {
    print_statsline_header();
  }

  int r = EXIT_SUCCESS;
  for(size_t i = 0; i < filenames_count; ++i) {
    r = dofile(filenames[i],
               printnames[i],
               outnames[i],
               i,
               copy,
               remove_prefix,
               statsline,
               comments);
    if(r != EXIT_SUCCESS) {
      fprintf(stderr, "%s\tError!\n", filenames[i]);
    }
  }

  if(print_arith_mean_at_end)
    print_arithmetic_means_from_totals();

  free(filenames);
  free(printnames);

  return r;
}
